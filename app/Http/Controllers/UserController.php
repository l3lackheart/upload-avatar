<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class UserController extends Controller
{
    /**
     * Show upload avatar form
     */
    public function showUploadForm() {
        $data = [];

        $data['user'] = Auth::user();
        return view('change_avatar', $data)->render();
    }

    /**
     * Handle image uploaded
     */
    public function uploadImage(Request $request) {
        $user = Auth::user();
        if (($request->hasFile('avatar'))) {
            $destinationPath = public_path('media/user/avatar'); // upload path
            $extension = $request->file('avatar')->getClientOriginalExtension(); // getting image extension

            $tempName = $request->file("avatar")->getClientOriginalName();
            $fileName = $user->id . '.' . $extension; // renameing image
            $request->file('avatar')->move($destinationPath, $fileName); // uploading file to given path

            $user->avatar = asset('media/user/avatar/'.$fileName); 
            $user->save();   
        }
    }
}
